/*
Array management functions
*/

export function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
  
    while (0 !== currentIndex) {
  
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
  
    return array;
};

export function zip(source1, source2) {

    if (source1.length !== source2.length) {
        const tempSource1 = [...source1];
        const tempSource2 = [...source2];

        for (let i=0; i < tempSource2.length - 1; i++) {
            source1.push(...tempSource1);
        }
        
        for (let i=0; i < tempSource1.length - 1; i++) {
            source2.push(...tempSource2);
        }
    }

    var result=[];
    source1.forEach(function(o,i){
        result.push(o);
        result.push(source2[i]);
    });
    return (result.filter(item => item !== undefined));
}


export function timeToSecs(minSecString) {
    const minSecArr = minSecString.split(":");
    const secs = parseInt((minSecArr[0] * 60), 10) + parseInt(minSecArr[1], 10);
    return secs; 
}