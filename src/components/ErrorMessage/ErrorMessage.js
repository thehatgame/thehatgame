import React from 'react';
import './ErrorMessage.css';

function ErrorMessage(props) {

    const { errorCode } = props;

    function getErrorMessage() {
        switch(errorCode) {
            case 'anonymous-auth-failed':
                return 'Anonymous authentication failed. Try again.'
            case 'room-not-found':
                return 'The room could not be found. Try creating a new one.';
            case 'room-get-fail':
                return 'Failed to find the room. Try again.';
            case 'invalid-team-ref':
                return 'Team reference is invalid.';
            case 'create-room-error':
                return 'Failed to create the room. Try again.';
            case 'add-user-to-room-error':
                return 'Failed to add user to room. Try again.';
            case 'entry-text-req':
                return 'Entry text required';
            case 'entry-too-long':
                return 'Entry must be fewer than 50 characters.';
            case 'entry-limit':
                return "You've reached the entry limit!";
            case 'duplicate-entry-error':
                return 'Entry in hat already';
            case 'user-name-required':
                return 'Your name is required';
            case 'user-name-exists':
                return 'Name is already taken';
            case 'user-name-too-long':
                return 'Name must be fewer than 20 characters.'
            case 'entries-get-fail':
                return 'Failed to get hat entries';
            case 'user-list-get-fail':
                return 'Failed to retrive users'
            case 'room-code-required':
                return 'A valid room code is required'
            case 'ready-room-error':
                return 'Unable to ready up room. Try Again.';
            case 'choose-team-error':
                return 'Unable to choose team. Try Again.';
            case 'current-player-get-fail':
                return 'Unable to get current player.';
            case 'join-room-error':
                return 'Unable to find room. Try direct link.';
            case 'no-entries':
                return 'At least one entry needs to be in the hat!';
            case 'empty-team':
                return 'Both teams must have at least one player (preferably more!)';
            default:
                return 'Oops, something went wrong.';
        }
    }

    return errorCode ? <p className="error">{getErrorMessage()}</p> : null;
};

export default ErrorMessage;