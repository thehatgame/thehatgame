const teamAData = {
    team: "team-a",
    color: "orange",
    text: "Team A"
}

const teamBData = {
    team: "team-b",
    color: "purple",
    text: "Team B"
}

export default function TeamToData(team) {
    if (team === "team-a") {
        return teamAData;
    } else {
        return teamBData;
    }
}