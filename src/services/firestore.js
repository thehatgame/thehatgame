import * as firebase from "firebase/app";
import "firebase/firestore";
import "firebase/functions";
import "firebase/auth";

const firebaseConfig = {
    apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
    authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL,
    projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
    storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_FIREBASE_APP_ID,
    measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENT_ID
  };

firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();


export const authenticateAnonymously = () => {
    return firebase.auth().signInAnonymously();
};

/* AUTHENTICATED, UNREGISTERED IN USERS COLLECTION */
/*            landing page, enter room             */

export const createRoom = (userId) => {
    return db.collection('rooms')
        .add({
            created: firebase.firestore.FieldValue.serverTimestamp(),
            createdBy: userId,
        });
}

export const primeRoom = (roomId, shortId) => {

    db.collection('rooms')
        .doc(roomId)
        .update({
            shortId: shortId
        });

    db.collection('rooms')
        .doc(roomId)
        .collection('statusDocs')
        .doc('gameStatus')
        .set({
            currentPlayerNum: 0,
            readEntriesIndexes: [],
            currentRound: 1,
            teamAScore: 0,
            teamBScore: 0,
            gameOver: false,
            extraTime: 0,
            turnsTillExtraTime: -1
        });

    return db.collection('rooms')
        .doc(roomId)
        .collection('statusDocs')
        .doc('roomStatus')
        .set({
            players: 0,
            entries: 0,
            readyToPlay: false
        })

}

export const getRoom = roomId => {
    return db.collection('rooms')
        .doc(roomId)
        .get();
}

export const getRoomByShortId = shortId => {
    return db.collection('rooms')
        .orderBy("created", "desc")
        .where("shortId", '==', shortId)
        .get()
}

export const addUserToRoom = (userName, userId, roomId, team) => {
    db.collection("rooms")
        .doc(roomId)
        .collection("statusDocs")
        .doc("roomStatus")
        .update({
            players: firebase.firestore.FieldValue.increment(1)
        });
    return db.collection('rooms')
        .doc(roomId)
        .collection('users')
        .doc(userId)
        .set({
            name: userName,
            team: team,
            id: userId
        });
}

export const streamUsers = (roomId, observer) => {
    return db.collection('rooms')
        .doc(roomId)
        .collection('users')
        .orderBy('team')
        .onSnapshot(observer);
}

/* AUTHENTICATED, REGISTERED IN USERS COLLECTION */
/*                  fill hat                     */

export const streamRoomStatus = (roomId, observer) => {
    return db.collection('rooms')
        .doc(roomId)
        .collection('statusDocs')
        .doc('roomStatus')
        .onSnapshot(observer)
}

export const getHatEntries = roomId => {
    return db.collection('rooms')
        .doc(roomId)
        .collection('hatEntries')
        .get()
}

export const addHatEntry = (entryText, roomId) => {
    return getHatEntries(roomId)
        .then(querySnapshot => querySnapshot.docs)
        .then(hatEntries => hatEntries.find(hatEntry => hatEntry.data().text.toLowerCase() === entryText.toLowerCase()))
        .then (matchingItem => {
            if (!matchingItem) {
                db.collection('rooms')
                    .doc(roomId)
                    .collection('statusDocs')
                    .doc('roomStatus')
                    .update({entries: firebase.firestore.FieldValue.increment(1)})

                return db.collection('rooms')
                    .doc(roomId)
                    .collection('hatEntries')
                    .add({
                        text: entryText,
                        created: firebase.firestore.FieldValue.serverTimestamp()
                    })
            }
            throw new Error('duplicate-entry-error');
        });
}

export const readyUpRoom = roomId => {
    return db.collection('rooms')
        .doc(roomId)
        .collection('statusDocs')
        .doc('roomStatus')
        .update({
            readyToPlay: true
        });
}

export const getUsers = roomId => {
    return db.collection('rooms')
        .doc(roomId)
        .collection('users')
        .get()
}

/* AUTHENTICATED, REGISTERED IN USERS COLLECTION */
/*                  games room                   */

export const streamGameStatus = (roomId, observer) => {
    return db.collection('rooms')
        .doc(roomId)
        .collection('statusDocs')
        .doc('gameStatus')
        .onSnapshot(observer)
}

export const updateGameStatus = (roomId, playerNum, completedEntries, teamAScoreModifier, teamBScoreModifier) => {
    // runs for player incrementation
    if (completedEntries === null) {
        // reset num to splice
        return db.collection('rooms')
        .doc(roomId)
        .collection('statusDocs')
        .doc('gameStatus')
        .update({
            currentPlayerNum: playerNum,
            readEntriesIndexes: [],
            teamAScore: firebase.firestore.FieldValue.increment(teamAScoreModifier),
            teamBScore: firebase.firestore.FieldValue.increment(teamBScoreModifier),
            turnsTillExtraTime: firebase.firestore.FieldValue.increment(-1)
        })
    } else if (completedEntries.length === 0) {
        // do nothing to num to splice
        return db.collection('rooms')
        .doc(roomId)
        .collection('statusDocs')
        .doc('gameStatus')
        .update({
            currentPlayerNum: playerNum,
            teamAScore: firebase.firestore.FieldValue.increment(teamAScoreModifier),
            teamBScore: firebase.firestore.FieldValue.increment(teamBScoreModifier),
            turnsTillExtraTime: firebase.firestore.FieldValue.increment(-1)
        })

    } else {
        // increment num to splice
        return db.collection('rooms')
        .doc(roomId)
        .collection('statusDocs')
        .doc('gameStatus')
        .update({
            currentPlayerNum: playerNum,
            readEntriesIndexes: firebase.firestore.FieldValue.arrayUnion(...completedEntries),
            teamAScore: firebase.firestore.FieldValue.increment(teamAScoreModifier),
            teamBScore: firebase.firestore.FieldValue.increment(teamBScoreModifier),
            turnsTillExtraTime: firebase.firestore.FieldValue.increment(-1)
        })
    }
}

export const incrementRoundNum = (roomId) => {
    return db.collection('rooms')
        .doc(roomId)
        .collection('statusDocs')
        .doc('gameStatus')
        .update({
            currentRound: firebase.firestore.FieldValue.increment(1)
        })
}

export const setExtraTime = (roomId, extraTime) => {
    return db.collection('rooms')
        .doc(roomId)
        .collection('statusDocs')
        .doc('gameStatus')
        .update({
            extraTime: extraTime,
            turnsTillExtraTime: 2
        });
}

export const setGameOver = (roomId) => {
    db.collection('rooms')
        .doc(roomId)
        .update({
            deletable: true
        });
    return db.collection('rooms')
        .doc(roomId)
        .collection('statusDocs')
        .doc('gameStatus')
        .update({
            gameOver: true
        })
}

export const getGameStatus = (roomId) => {
    return db.collection('rooms')
        .doc(roomId)
        .collection('statusDocs')
        .doc('gameStatus')
        .get();
}

export const deleteAtPath = path => {
    var deleteFn = firebase.functions().httpsCallable('recursiveDelete');
    
    deleteFn({ path: path })
        .then(function(result) {
            //console.log('Delete success: ' + JSON.stringify(result));
        })
        .catch(function(err) {
            console.log('Delete failed, see console,');
            console.warn(err);
        });
}
