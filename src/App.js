import React, { useState, useEffect } from 'react';

import * as FirestoreService from './services/firestore';
import * as ArrayFuncs from './components/ArrayFunctions';

import LandingPage from './scenes/LandingPage/LandingPage';
import EnterRoom from './scenes/EnterRoom/EnterRoom';
import FillHat from './scenes/FillHat/FillHat';
import ErrorMessage from './components/ErrorMessage/ErrorMessage';
import GamesRoom from './scenes/GamesRoom/GamesRoom';
import EndScreen from './scenes/EndScreen/EndScreen';

import useQueryString from './hooks/useQueryString'


function App() {

  const [user, setUser] = useState();
  const [userId, setUserId] = useState();
  const [room, setRoom] = useState();
  const [error, setError] = useState();
  const [scores, setScores] = useState([]);
  const [entries, setEntries] = useState();
  const [isGameRoom, setIsGameRoom] = useState();
  const [roomOwnerId, setRoomOwnerId] = useState();
  const [showEndScreen, setShowEndScreen] = useState(false);
  const [playerOrderList, setPlayerOrderList] = useState();

  // Use a custom hook to subscribe to the grocery list ID provided as a URL query parameter
  const [roomId, setRoomId] = useQueryString('roomId');

  // Use an effect to authenticate and load the grocery list from the database
  useEffect(() => {
    FirestoreService.authenticateAnonymously().then(userCredential => {
      setUserId(userCredential.user.uid);
      if (roomId) {
        FirestoreService.getRoom(roomId)
          .then(room => {
            if (room.exists) {
              setError(null);
              setRoom(room.data());
              setRoomOwnerId(room.data().createdBy);
              
            } else {
              setError('room-not-found');
              setRoomId();
            }
          })
          .catch(() => setError('room-get-fail'));
      }
    })
    .catch(() => setError('anonymous-auth-failed'));
  }, [roomId, setRoomId, roomOwnerId, setRoomOwnerId]);

  function onCreateRoom(roomId) {
    setRoomId(roomId);
    setRoomOwnerId(userId);
  }

  function onEnterRoom(userName, team) {
    FirestoreService.getRoom(roomId)
      .then(docRef => {
        setRoom(docRef.data());
        setUser({name: userName, team: team});
      })
  }

  function onJoinRoom(roomId) {
    setRoomId(roomId)
  }

  function onCloseRoom() {
    FirestoreService.getRoom(roomId)
      .then(docRef => {
        if (docRef.exists) {
          const path = '/rooms/' + roomId;
          FirestoreService.deleteAtPath(path);
        }
      });
    setShowEndScreen(false);
    setIsGameRoom(false);
    setEntries();
    setScores();
    setRoomId();
    setRoom();
    setUser();
  }

  function onGameOver() {
    FirestoreService.getGameStatus(roomId)
      .then(docRef => {
        setScores([docRef.data().teamAScore, docRef.data().teamBScore]);
        setShowEndScreen(true);
        setIsGameRoom(false);
      })
  }

  function onReadyStatusChange(isReady) {
    if (isReady) {
      FirestoreService.getUsers(roomId)
        .then(querySnapshot => {

          // create random order team-alternating player list
          const usersArray = querySnapshot.docs.map(docSnapshot => docSnapshot.data());
          const aArray = usersArray.filter(user => user.team === "team-a");
          const bArray = usersArray.filter(user => user.team === "team-b");

          const zippedPlayerList = ArrayFuncs.zip(aArray, bArray);
          setPlayerOrderList(zippedPlayerList);

          FirestoreService.getHatEntries(roomId)
            .then(querySnapshot => {
              var Chance = require('chance');
              var roomIdSeed = new Chance(roomId);
              var ownerIdSeed = new Chance(room.createdBy);
                const entriesArray = querySnapshot.docs.map(docSnapshot => docSnapshot.data());

                var roundOneEntries = [...entriesArray];
                roundOneEntries = roundOneEntries.map((entry, index) => {return {text: entry.text, created: entry.created, ogIndex: index}})
                var roundTwoEntries = roomIdSeed.shuffle([...entriesArray]);
                roundTwoEntries = roundTwoEntries.map((entry, index) => {return {text: entry.text, created: entry.created, ogIndex: index}})
                var roundThreeEntries = ownerIdSeed.shuffle([...entriesArray]);
                roundThreeEntries = roundThreeEntries.map((entry, index) => {return {text: entry.text, created: entry.created, ogIndex: index}})

                setEntries([roundOneEntries, roundTwoEntries, roundThreeEntries]);
                setIsGameRoom(isReady);

            }).catch(() => setError('entries-get-fail'));

        }).catch(() => setError('user-list-get-fail'));
    }
  }
  
  // render a scene based on the current state
  if (showEndScreen) {
    return (
      <EndScreen {...{onCloseRoom, scores, user}}></EndScreen>
    )
  } else if (room && user && isGameRoom) {
    return (
      <GamesRoom endGame={onGameOver} { ...{roomId, userId, entries, playerOrderList}}></GamesRoom>
    )
  } else if (room && user) {
    return (
      <FillHat onReady={onReadyStatusChange} {...{ room, roomId, onCloseRoom, userId, roomOwnerId, user}}></FillHat>
    );
  } else if (room) {
    return (
      <div>
        <ErrorMessage errorCode={error}></ErrorMessage>
        <EnterRoom onEnter={onEnterRoom} {...{roomId, onCloseRoom, userId}}></EnterRoom>
      </div>
    );
  }
  return (
    <div>
      <LandingPage onCreate={onCreateRoom} onJoin={onJoinRoom} userId={userId}></LandingPage>
    </div>
  );
}

export default App;
