import React, { useState } from 'react';
import { Input, Segment } from 'semantic-ui-react'

import './AddEntry.css';
import * as FirestoreService from '../../../services/firestore';
import ErrorMessage from '../../../components/ErrorMessage/ErrorMessage'


function AddEntry(props) {

    const { roomId, numEntries } = props;

    const [error, setError] = useState('');
    const [input, setInput] = useState('');


    function addEntry(e) {
        e.preventDefault();
        setError(null);

        const entryText = input;
        if (!entryText) {
            setError('entry-text-req');
            return;
        }
        if (entryText.length > 50) {
            setError('entry-too-long');
            return;
        }
        if (numEntries === 70) {
            setError('entry-limit');
            return;
        }

        FirestoreService.addHatEntry(entryText, roomId)
            .then(() => setInput(''))
            .catch(reason => {
                if (reason.message === 'duplicate-entry-error') {
                    setError(reason.message);
                } else {
                    setError('add-hat-entry-error');
                }
            });
    }

    return (
        <Segment inverted>
            <h3>Add to the hat!</h3>
            <Input 
                action={{ color: 'blue', content: 'Add', type: 'submit', onClick: (e) => addEntry(e)}}
                type="text" 
                name="entryText" 
                value={input}
                placeholder="Name" 
                onChange={event => setInput(event.target.value)}  />
            
            <ErrorMessage errorCode={error}></ErrorMessage>
        </Segment>
    );

}

export default AddEntry;