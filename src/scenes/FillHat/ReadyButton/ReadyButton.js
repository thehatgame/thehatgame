import React, { useState } from 'react';
import { Button, Confirm, Divider, Message } from 'semantic-ui-react'

import * as FirestoreService from '../../../services/firestore';
import ErrorMessage from '../../../components/ErrorMessage/ErrorMessage';

function ReadyButton(props) {

    const { userId, roomOwnerId, roomId, numEntries } = props;
    const [ isOpen, setIsOpen ] = useState();

    const [ error, setError ] = useState();


    function readyRoom(e) {
        e.preventDefault();
        setError(null);

        FirestoreService.readyUpRoom(roomId)
            .catch(() => setError('ready-room-error'));
   
    }

    function checkEntryNum(e) {
        e.preventDefault();
        if (numEntries === 0) {
            setError('no-entries');
            return;
        } else {
            FirestoreService.getUsers(roomId)
                .then(querySnapshot => {
                    const usersArray = querySnapshot.docs.map(docSnapshot => docSnapshot.data());
                    const aArray = usersArray.filter(user => user.team === "team-a");
                    const bArray = usersArray.filter(user => user.team === "team-b");
                    if (aArray.length === 0 || bArray.length === 0) {
                        setError('empty-team');
                        return;
                    } else {
                        setIsOpen(true);
                    }
                })
        }
                
    }

                            
    if (roomOwnerId === userId) {
        return (
            <div>
                <Divider inverted />
                <Button
                    size="large"
                    color="teal" 
                    onClick={checkEntryNum}>Ready!</Button
                >
                <Confirm
                    open={isOpen}
                    header="Are you sure everyone's in?"
                    content=""
                    cancelButton='Never mind'
                    confirmButton="Let's go"
                    onCancel={() => setIsOpen(false)}
                    onConfirm={readyRoom}
                />
                <ErrorMessage errorCode={error}></ErrorMessage>
            </div>
        )
    } else {
        return (
            <div>
                <Divider inverted />
                <Message info >Waiting for the room owner to ready up</Message>
            </div>
        )
    }
}

export default ReadyButton;