import React, { useEffect, useState } from 'react';
import { Segment, Message, Button, Popup } from 'semantic-ui-react'
import {useSpring, animated} from 'react-spring';

import TeamToData from '../../components/TeamToData';
import * as FirestoreService from '../../services/firestore';
import './FillHat.css';
import AddEntry from './AddEntry/AddEntry';
import ReadyButton from './ReadyButton/ReadyButton';
import ErrorMessage from '../../components/ErrorMessage/ErrorMessage';

function FillHat(props) {

    const { room, roomId, onCloseRoom, userId, roomOwnerId, onReady, user } = props;

    const [ numEntries, setNumEntries ] = useState();
    const [ error, setError ] = useState();

    const fadeInFirst = useSpring({config: {duration: 300}, from: {opacity: 0}, to: {opacity: 1}, delay: 100});
    const fadeInSecond = useSpring({config: {duration: 300}, from: {opacity: 0}, to: {opacity: 1}, delay: 300});

    useEffect(() => {
        const unsubscribe = FirestoreService.streamRoomStatus(roomId, {
            next: doc => {
                if (doc.exists) {
                    setNumEntries(doc.data().entries);
                    onReady(doc.data().readyToPlay);
                };
            },
            error: () => setError('ready-status-get-fail')
        });
        return unsubscribe;
    }, [roomId, onReady]);

    function returnToLandingPage(e) {
        e.preventDefault();
        onCloseRoom();
    }

    function setClipboard(value) {
        var tempInput = document.createElement("input");
        tempInput.style = "position: absolute; left: -1000px; top: -1000px";
        const linkvalue = "https://thehatgame.page.link/?link=" + window.location.href + "&apn=com.solstice.thehatgame&amv=4"
        tempInput.value = linkvalue;
        document.body.appendChild(tempInput);
        tempInput.select();
        document.execCommand("copy");
        document.body.removeChild(tempInput);
    }

    return (
        <div className="wrapper">
            <header className="page-header">
                <h2>In Room {room.shortId}</h2>
            </header>
            <animated.div style={fadeInFirst} className="page-main">
                <Message
                    color={TeamToData(user.team).color}>
                    You are on {TeamToData(user.team).text}
                </Message>
                <Segment.Group raised>
                    <AddEntry {...{roomId, numEntries}}></AddEntry>
                    <Segment inverted>
                        <div className="num-entries-display">
                            <h1>
                                {numEntries} entries
                            </h1>
                        </div>
                    </Segment>
                </Segment.Group>

                <ReadyButton {...{userId, roomOwnerId, roomId, numEntries}}></ReadyButton>
                <ErrorMessage errorcode={error}></ErrorMessage>
            </animated.div>
            
            <animated.footer style={fadeInSecond} className="page-footer">
                <p>Share  
                    <Popup 
                        on='click'
                        mouseLeaveDelay={500}
                        hideOnScroll 
                        content='Link copied!'
                        inverted
                        trigger={<Button compact circular secondary icon='share alternate' onClick={setClipboard} />}

                    /> 
                    your room with others or 
                    <Button compact circular secondary icon='home' onClick={returnToLandingPage} /> 
                    return to landing page.
                </p>
            </animated.footer>    
        </div>
    );
    
}

export default FillHat;