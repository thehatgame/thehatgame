import React, { useState } from 'react';
import { Button, Input, Segment, Divider } from 'semantic-ui-react';
import {useSpring, animated} from 'react-spring';

import './EnterRoom.css';
import ErrorMessage from '../../components/ErrorMessage/ErrorMessage';
import * as FirestoreService from '../../services/firestore';
import UserList from './UserList/UserList';

function EnterRoom(props) {

    const { roomId, onCloseRoom, onEnter, userId } = props;

    const [ error, setError ] = useState();
    const [ userName, setUserName ] = useState();

    const fadeInFirst = useSpring({config: {duration: 300}, from: {opacity: 0}, to: {opacity: 1}, delay: 100});
    const fadeInSecond = useSpring({config: {duration: 300}, from: {opacity: 0}, to: {opacity: 1}, delay: 300});

    function addNewUser(team) {
        setError(null);

        if (!userName) {
            setError('user-name-required');
        } else if (userName.length > 20) {
            setError('user-name-too-long');
        } else {
            FirestoreService.addUserToRoom(userName, userId, roomId, team)
                .catch(() => setError('add-user-to-room-error'));
            
                onEnter(userName, team)
        }
    }

    function handleChange(e) {
            setUserName(e.target.value);
    }

    function returnToLandingPage(e) {
        e.preventDefault();
        onCloseRoom();
    }

    return (
        <div className="wrapper">
                <header className="page-header">
                    <h2>Joining room...</h2>
                </header>

            <animated.div style={fadeInFirst} className="page-main">
                <Segment placeholder inverted>
                    <div className="users-column">
                        <UserList {...{roomId, userId}}></UserList>
                    </div>
                </Segment>

                <Divider  inverted />

                <div>
                    <p> Enter your name and choose a team! </p>
                    <Input type="text" name="name" placeholder="Name" onChange={handleChange}/>
                    <Button.Group>
                        <Button color='orange' onClick={() => addNewUser("team-a")}>Team A</Button>
                        <Button.Or />
                        <Button color='violet' onClick={() => addNewUser("team-b")}>Team B</Button>
                    </Button.Group>
                    <ErrorMessage errorCode={error}></ErrorMessage>
                </div>
            </animated.div>

            <animated.footer style={fadeInSecond} className="page-footer">
                ...or 
                <Button compact circular secondary icon='home' onClick={returnToLandingPage} /> 
                return to landing page.
            </animated.footer>
        </div>
    );
}

export default EnterRoom;