import React, { useEffect, useState } from 'react';
import { Segment, List } from 'semantic-ui-react'

import './UserList.css'
import * as FirestoreService from '../../../services/firestore';
import ErrorMessage from '../../../components/ErrorMessage/ErrorMessage';

function UserList(props) {

    const { roomId } = props;

    const [ users, setUsers ] = useState([]);
    const [ error, setError ] = useState();

    // Use an effect hook to subscribe to the users stream and
    // automatically unsubscribe when the component unmounts.
    useEffect(() => {
        const unsubscribe = FirestoreService.streamUsers(roomId, {
            next: querySnapshot => {
                const updatedUserList = 
                    querySnapshot.docs.map(docSnapshot => docSnapshot.data());
                setUsers(updatedUserList);
            },
            error: () => setError('user-list-get-fail')
        });
        return unsubscribe;
    }, [roomId, setUsers]);

    const getTeamList = (team) => {
        const teamList = users.filter(user => user.team === team);
        return teamList.map((user, i) => 
            <List.Item 
                key={i}>
                {user.name}</List.Item>);
    }

    return (
        <Segment.Group>
            <Segment inverted>
                <h3>Players in room</h3>
            </Segment>
            <Segment.Group horizontal>
                <Segment inverted>
                    <h4>Team A</h4>
                    <List>
                        {getTeamList("team-a")}
                    </List>
                </Segment>
                <Segment inverted>
                    <h4>Team B</h4>
                    <List>
                        {getTeamList("team-b")}
                    </List>
                </Segment>
            </Segment.Group>
            <ErrorMessage errorCode={error}></ErrorMessage>
        </Segment.Group>
    );

    /*return (
        <div>
            <p>Players in room</p>
            <ErrorMessage errorCode={error}></ErrorMessage>
            <div className="team-lists">
                <div className="a-users">{getTeamList("team-a")}</div>
                <Divider inverted vertical />
                <div className="b-users">{getTeamList("team-b")}</div>
            </div>
            
        </div>
    );*/
}

export default UserList;