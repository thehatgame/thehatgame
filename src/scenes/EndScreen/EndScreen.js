import React from 'react';
import {useSpring, animated} from 'react-spring'
import { Button, Segment, Message } from 'semantic-ui-react'

import './EndScreen.css'


function EndScreen(props) {

    const { onCloseRoom, scores , user } = props;

    const fadeInThird = useSpring({config: {duration: 300}, from: {opacity: 0}, to: {opacity: 1}, delay: 4000})


    function returnToLandingPage(e) {
        e.preventDefault();
        onCloseRoom();
    }

    const winningTeam = (scores[0] > scores[1]) ? {team: "team-a", color: "orange"} : {team: "team-b", color: "purple"};
    const losingTeam = (scores[0] < scores[1]) ? {team: "team-a", color: "orange"} : {team: "team-b", color: "purple"};


    const endMessage = () => {
        if (scores[0] === scores[1]) {
            return (
                <Message 
                    size="massive"
                    color="teal">
                    Its a draw!
                </Message>
            )
        } else if (user.team === winningTeam.team) {
            return (
                <Message 
                    size="massive"
                    color={winningTeam.color}>
                    Congratulations!
                </Message>
            )
        } else {
            return (
                <Message 
                    size="massive"
                    color={losingTeam.color}>
                    Better luck next time!
                </Message>
            )
        }
    }

    const animatedAScore = useSpring({
        config: {mass:500, tension:20, friction:500, velocity:scores[0], clamp: true},
        from: { val: 0},
        to: {val: scores[0]}
    });
    const animatedBScore = useSpring({
        config: {mass:500, tension:20, friction:500, velocity:scores[1], clamp: true},
        from: { val: 0 },
        to: {val: scores[1]}
    });


    return (
        <div className="wrapper">
            <header className="page-header">
                <h1>The winner is...</h1>
            </header>
            <div className="page-main">
                <Segment inverted>
                    <Segment.Group>
                        <Segment inverted>
                            <h3>Scores</h3>
                        </Segment>
                        <Segment.Group horizontal className="scores-container">
                            <Segment inverted>
                                <h4>Team A</h4>
                                <animated.div className="score-display">
                                    {animatedAScore.val.interpolate(val => Math.floor(val))}
                                </animated.div>
                            </Segment>
                            <Segment inverted>
                                <h4>Team B</h4>
                                <animated.div className="score-display">
                                    {animatedBScore.val.interpolate(val => Math.floor(val))}
                                </animated.div>
                            </Segment>
                        </Segment.Group>
                    </Segment.Group>
                </Segment>

                <animated.div style={fadeInThird}>
                    {endMessage()}
                    <Button 
                        size="large"
                        color="teal" 
                        onClick={returnToLandingPage}>Landing page</Button>
                </animated.div>

            </div>
        </div>
    )
}

export default EndScreen;