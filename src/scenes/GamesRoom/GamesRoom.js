import React, { useState, useEffect } from 'react';
import { Button, Card, Segment, Message } from 'semantic-ui-react';
import { useSprings, useSpring, animated, interpolate } from 'react-spring';
import CountdownTimer from "react-component-countdown-timer";


import * as FirestoreService from '../../services/firestore';
import TeamToData from '../../components/TeamToData';
import ErrorMessage from '../../components/ErrorMessage/ErrorMessage';
import * as ArrayFuncs from '../../components/ArrayFunctions';
import './GamesRoom.css';


function GamesRoom(props) {

    const { roomId, userId, entries, playerOrderList, endGame } = props;
    const [error, setError] = useState();
    
    const [teamAScore, setTeamAScore] = useState(0);
    const [teamBScore, setTeamBScore] = useState(0);
    const [currentPlayerNum, setCurrentPlayerNum] = useState(0);
    const [roundNum, setRoundNum] = useState(1);
    const [turnLength, setTurnLength] = useState(50);
    const [playerPasses, setPlayerPasses] = useState(2);
    const [roundUnreadEntries, setRoundUnreadEntries] = useState([...entries[roundNum-1]]);
    const [inbetweenRounds, setInbetweenRounds] = useState(true);
    const [playerIsReady, setPlayerIsReady] = useState(false);
    const [gone] = useState(() => new Set());
    const [pass] = useState(() => new Set());


    useEffect(() => {
        setRoundUnreadEntries([...entries[roundNum-1]]);
        setPlayerIsReady(false);

        const unsubscribe = FirestoreService.streamGameStatus(roomId, {
            next: doc => {
                if (doc.exists) {
                    if (doc.get("currentPlayerNum") !== null) {
                        if (doc.data().gameOver) {
                            endGame();
                        }

                        if (roundNum < doc.data().currentRound) {
                            setRoundNum(doc.data().currentRound);
                        }

                        var tempEntries = [...entries[doc.data().currentRound-1]];
                        if (doc.data().readEntriesIndexes.length > 0) {
                            for (let i=0; i < doc.data().readEntriesIndexes.length; i++) {
                                tempEntries = tempEntries.filter(entry => entry.ogIndex !== doc.data().readEntriesIndexes[i]);
                                };
                            // shuffle array
                            ArrayFuncs.shuffle(tempEntries);
                        }
                        
                        if (doc.data().turnsTillExtraTime === 0) {
                            setTurnLength(50 + doc.data().extraTime);
                        } else {
                            setTurnLength(50);
                        }

                        setRoundUnreadEntries(tempEntries);
                        setCurrentPlayerNum(doc.data().currentPlayerNum);
                    } 
                }
            },
            error: () => setError('current-player-get-fail')
        });
        return unsubscribe;
    }, [setCurrentPlayerNum, entries, roundNum, setRoundUnreadEntries, endGame, roomId, playerOrderList])

    const nextEntry = () => {
        if (gone.size + pass.size < roundUnreadEntries.length - 1) {
            removeCard(roundUnreadEntries.length - gone.size - pass.size - 1);
            playerOrderList[currentPlayerNum].team === "team-a" ? setTeamAScore(teamAScore + 1) : setTeamBScore(teamBScore + 1);

        } else {
            removeCard(roundUnreadEntries.length - gone.size - pass.size - 1);
            if (pass.size === 0) {
                endRound();
            } else {
                nextPlayer()
            }
        }
    };

    const passEntry = () => {
        if (playerPasses > 0) {
            if (gone.size + pass.size < roundUnreadEntries.length - 1) {
                passCard(roundUnreadEntries.length - gone.size - pass.size - 1);
                setPlayerPasses(playerPasses - 1);
            } else {
                passCard(roundUnreadEntries.length - gone.size - pass.size - 1);
                setPlayerPasses(playerPasses - 1);
                nextPlayer();
            }
        }
    };
    
    const endRound = () => {
        const currentTeam = playerOrderList[currentPlayerNum].team;

        const timerText = document.getElementById("countdown-timer").textContent;
        FirestoreService.setExtraTime(roomId, ArrayFuncs.timeToSecs(timerText));

        setInbetweenRounds(true);
        setPlayerIsReady(false);

        currentTeam === "team-a" ? nextPlayer(null, teamAScore+1, teamBScore) : nextPlayer(null, teamAScore, teamBScore+1);
        set(i => to(i));

        if (roundNum < 3) {
            FirestoreService.incrementRoundNum(roomId);
        } else {
            FirestoreService.setGameOver(roomId);
        }
    };

    const nextPlayer = (completedEntriesOgIndexes=Array.from(gone), teamAModifier=teamAScore, teamBModifier=teamBScore) => {
        let newPlayerNum;
        if (currentPlayerNum < playerOrderList.length - 1) {
            newPlayerNum = currentPlayerNum + 1;
        } else {
            newPlayerNum = 0;
        }

        setPlayerIsReady(false);
        FirestoreService.updateGameStatus(roomId, newPlayerNum, completedEntriesOgIndexes, teamAModifier, teamBModifier);
        
        gone.clear();
        pass.clear();
        set(i => to(i));
        setTeamAScore(0);
        setTeamBScore(0);
        setPlayerPasses(2);
        
    };

    // CARD STACK ANIMATION AND HANDLING

    const to = i => ({ x: 0, y: i * -0.5, scale: 1, rot: -10 + Math.random() * 20, delay: i * 2 })
    const from = i => ({ x: 0, y: i * -0.5, scale: 1, rot: 0,   });
    const trans = (r, s) => `perspective(0px) rotateX(30deg) rotateY(${r / 10}deg) rotateZ(${r}deg) scale(${s})`;

    const [springs, set] = useSprings(roundUnreadEntries.length, i => ({ ...to(i), from: from(i) }));
    const fadeInFirst = useSpring({config: {duration: 300}, from: {opacity: 0}, to: {opacity: 1}, delay: 100});

    const removeCard = index => {
        const dir = 1;
        gone.add(roundUnreadEntries[index].ogIndex);
        set(i => {
          if (index !== i) return
          const isGone = gone.has(roundUnreadEntries[index].ogIndex)
          const x = (200 + window.innerWidth) * dir
          const rot = (isGone ? dir * 10 : 0) 
          const scale = 1.1
          return { x, rot, scale, delay: undefined, config: { friction: 50, tension: 200 } }
        })
    };

    const passCard = index => {
        const dir = -1;
        pass.add(roundUnreadEntries[index].ogIndex);
        set(i => {
          if (index !== i) return
          const isGone = pass.has(roundUnreadEntries[index].ogIndex)
          const x = (200 + window.innerWidth) * dir
          const rot = (isGone ? dir * 10 : 0) 
          const scale = 1.1
          return { x, rot, scale, delay: undefined, config: { friction: 50, tension: 200 } }
        })
    };

    const AnimatedCard = animated(Card);

    const cardStack = () => { 
        return springs.map(({ x, y, rot, scale }, i) => (
            <animated.div 
              key={i} 
              className="card-stack" 
              style={{ transform: interpolate([x, y], (x, y) => `translate3d(${x}px,${y}px,0)`) }}>
                <AnimatedCard 
                  className="card" 
                  style={{ transform: interpolate([rot, scale], trans) }}>
                    <h2 style={{fontFamily: "Architects Daughter"}} className="card-text">{roundUnreadEntries[i].text}</h2>
                </AnimatedCard>
            </animated.div>
        ))
    };

    const roundTextDict = {
        1: "In round 1, you must describe the person or character without using their name.",
        2: "Time to step things up! In round two, your descriptions can only be one word long!",
        3: "Last round! Best hope your video quality is good, because you now must act out each person/character. No words allowed!"
    }
    

    if ((typeof playerOrderList[currentPlayerNum] != undefined) && (userId === playerOrderList[currentPlayerNum].id) && playerIsReady) {
        return (
            <div className="wrapper">
                <header className="page-header">
                    <h1>Round {roundNum}</h1>
                </header>
                <animated.div style={fadeInFirst} className="page-main">
                    <div className="stack-outer">
                        {cardStack()}
                    </div>
                    <Segment inverted>
                        <div className="timer">
                            <CountdownTimer 
                                id="countdown-timer"
                                count={turnLength}
                                hideDay
                                hideHours
                                size={35}
                                color={'#EEEEEE'}
                                backgroundColor={"#1b1c1d"}
                                onEnd={() => nextPlayer()}/>
                        </div>
                        <div className="buttons">
                            <Button
                                    disabled={(playerPasses===0)}
                                    color="red" 
                                    onClick={passEntry}>Pass ({playerPasses})</Button>
                            <Button
                                    color="green" 
                                    onClick={nextEntry}>Got it!</Button>
                        </div>
                    </Segment>
                    <ErrorMessage errorCode={error}></ErrorMessage>
                    
                </animated.div>
            </div>
        )
    } else if ((typeof playerOrderList[currentPlayerNum] != undefined) && userId === playerOrderList[currentPlayerNum].id) {
        return (
            <div className="wrapper">
                <header className="page-header">
                    <h1>Round {roundNum}</h1>
                </header>
                <animated.div style={fadeInFirst} className="page-main">
                    <Segment inverted>
                        <Message attached
                            size="small"    
                            color={TeamToData(playerOrderList[currentPlayerNum].team).color}>
                            <h2>{playerOrderList[currentPlayerNum].name}, you're up!</h2>
                            {TeamToData(playerOrderList[currentPlayerNum].team).text}
                        </Message>
                        <div className="description">
                            <p>{inbetweenRounds && roundTextDict[roundNum]}</p>
                        </div>
                        <Button
                            color="teal" 
                            onClick={() => {setInbetweenRounds(false); setPlayerIsReady(true);}}>We're ready!</Button>
                    </Segment>
                </animated.div>
            </div>
        )
    } else {
        return (
            <div className="wrapper">
                <header className="page-header">
                    <h1>Round {roundNum}</h1>
                </header>
                <animated.div style={fadeInFirst} className="page-main">
                    <Segment inverted>
                        <Message attached
                            size="small"    
                            color={TeamToData(playerOrderList[currentPlayerNum].team).color}>
                            <h2>It's {playerOrderList[currentPlayerNum].name}'s go!</h2>
                            {TeamToData(playerOrderList[currentPlayerNum].team).text}
                        </Message>
                        <div className="description">
                            <p>{inbetweenRounds && roundTextDict[roundNum]}</p>
                        </div>
                    </Segment>
                </animated.div>
            </div>
        )
    }
    

}

export default GamesRoom;