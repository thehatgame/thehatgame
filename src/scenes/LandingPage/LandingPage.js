import React, { useState } from 'react';
import {useSpring, animated} from 'react-spring'
import { Button, Input, Segment, Divider, Icon, Header, Dimmer, Container } from 'semantic-ui-react'

import * as FirestoreService from '../../services/firestore';
import ErrorMessage from '../../components/ErrorMessage/ErrorMessage';
import './LandingPage.css';


function LandingPage(props) {

    const { onCreate, onJoin, userId } = props;

    const [ error, setError ] = useState();
    const [ active, setActive ] = useState(false);

    const fadeInFirst = useSpring({config: {duration: 300}, from: {opacity: 0}, to: {opacity: 1}, delay: 100})
    const fadeInSecond = useSpring({config: {duration: 300}, from: {opacity: 0}, to: {opacity: 1}, delay: 300})
    const fadeInThird = useSpring({config: {duration: 300}, from: {opacity: 0}, to: {opacity: 1}, delay: 600})

    function createRoom(e) {
        e.preventDefault();
        setError(null);


        FirestoreService.createRoom(userId)
            .then(docRef => {
                var Chance = require('chance');
                var chance = new Chance(docRef.id);
                const shortCode = chance.string({ length: 4, casing: 'upper', alpha: true, numeric: true });
                onCreate(docRef.id);
                FirestoreService.primeRoom(docRef.id, shortCode);
            })
            .catch(() => setError('create-room-error'));
    } 

    const toInputUppercase = e => {
        e.target.value = ("" + e.target.value).toUpperCase();
    };

    const isAlphaNumericCapital = str => {
        var code, i, len;
      
        for (i = 0, len = str.length; i < len; i++) {
          code = str.charCodeAt(i);
          if (!(code > 47 && code < 58) && // numeric (0-9)
              !(code > 64 && code < 91)) { // upper alpha (A-Z)
            return false;
          }
        }
        return true;
      };

    function joinRoom(e) {
        e.preventDefault();
        setError(null);

        const shortRoomId = document.joinRoomForm.shortRoomId.value;
        if (!shortRoomId || (shortRoomId.length !== 4 ) || (!isAlphaNumericCapital(shortRoomId))) {
            setError('room-code-required');
            return;
        }

        FirestoreService.getRoomByShortId(shortRoomId)
            .then(querySnapshot => {
                let idArray = [];
                querySnapshot.forEach(doc => {
                    if (doc.data().deletable === undefined) {
                        idArray.push(doc.id);
                    }
                })
                if (idArray.length !== 1) {
                    setError('join-room-error');
                } else {
                    onJoin(idArray[0]);
                }
            })
            .catch(reason => console.log(reason));
    }

    const AnimatedSegment = animated(Segment);
    const AnimatedDivider = animated(Divider);

    return (
        <div className="wrapper">
            <Dimmer.Dimmable as={animated.header} blurring dimmed={active} className="page-header">
                <div className="header-grid">
                    <h1 className="grid-center">TheHatGame</h1>
                    <div className="grid-right">
                        <Button size="small" circular secondary icon='question' onClick={() => setActive(true)} />
                    </div>
                </div>
            </Dimmer.Dimmable>
            <div className="page-main">
                <Dimmer.Dimmable as={AnimatedSegment} blurring dimmed={active} className="landing-card" style={fadeInFirst} raised inverted>
                    <Header icon>
                        <Icon name='user plus' />
                        Join a room
                    </Header>
                    <form name="joinRoomForm">
                        <Input 
                            action={{ color: 'blue', content: 'Join', onClick: joinRoom}}
                            type="text" 
                            name="shortRoomId" 
                            variant="filled" 
                            placeholder="Room code"
                            onInput={toInputUppercase}    
                            />
                    </form> 
                    <ErrorMessage errorCode={error}></ErrorMessage>
                </Dimmer.Dimmable>

                <Dimmer.Dimmable as={AnimatedDivider} blurring dimmed={active} style={fadeInSecond} horizontal inverted>Or</Dimmer.Dimmable>

                <Dimmer.Dimmable as={AnimatedSegment} blurring dimmed={active} className="landing-card" style={fadeInThird} raised inverted>
                    <Header icon>
                        <Icon name='plus square' />
                        Create a new room
                    </Header>
                    <Button 
                        size="large"
                        color="teal" 
                        onClick={createRoom}>Create</Button>
                </Dimmer.Dimmable>

                <Dimmer className="dimmer-wrapper" active={active} onClickOutside={() => setActive(false)} page>
                    <Container text>
                        <Icon name='question circle' size="big" />
                        <Header as='h3' inverted dividing >
                            What is the hat game?
                            <Header.Subheader>
                            The hat game is a description and performance game for 4 or more players, incorperating ideas from similar games such as
                            Charades and Articulate.
                            </Header.Subheader>
                        </Header>
                        <Header as='h3' inverted style={{textAlign: "left"}} >
                            How do I play?
                            <Header.Subheader className="dimmer-text">
                            Players are divided in to two teams, A and B. Before beginning, every player places names into the virtual hat. These can be names of celebrities, members of your own party, characters from a book or game - just be confident everyone knows who they are!
                                During each round, players will take turns drawing names from the hat, and attempt to describe them to their teammates. Every successful guess earns a point for the team. The team with the most points at the end wins!
                                <br></br>
                                <br></br>
                                Round one plays like Articulate, where the player who is drawing names cannot use the person's name to describe them.
                                In round two, you are only allowed to use one word to describe the person/character, so it is important you try to remember who was in the hat!
                                In the final round, you must act out the name, without saying any words at all, as in Charades. 
                            </Header.Subheader>
                        </Header>
                        <Header as='h4' inverted style={{textAlign: "center"}} >
                            Click on "Create Room" to get started, or enter a room code to join your friends!
                        </Header>
                    </Container>
                    <footer className="landing-footer">
                        <Button compact circular secondary icon='close' onClick={() => setActive(false)} />
                    </footer>
                </Dimmer>

            </div>
            <Dimmer.Dimmable as={animated.footer} blurring dimmed={active} style={fadeInThird} className="page-footer">
                <Header as='h4' inverted>v1.8 | <a href="https://thehatgame.gitlab.io/docs/">Privacy Policy</a></Header>
            </Dimmer.Dimmable>
        </div>
    )
}

export default LandingPage;